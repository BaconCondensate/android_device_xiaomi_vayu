
### PROPRIETARY VENDOR FILES
$(call inherit-product, vendor/xiaomi/vayu/vayu-vendor.mk)

### DALVIK
$(call inherit-product, frameworks/native/build/phone-xhdpi-2048-dalvik-heap.mk)

### PLATFORM
$(call inherit-product, device/xiaomi/vayu/platform.mk)

DEVICE_PATH := device/xiaomi/vayu

DEVICE_PACKAGE_OVERLAYS += \
    $(DEVICE_PATH)/overlay \
    $(DEVICE_PATH)/overlay-lineage

#include $(DEVICE_PATH)/device/*.mk